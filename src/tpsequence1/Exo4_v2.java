
package exercices;

import java.util.Scanner;

public class Exo4_v2 {
 
    public static void main(String[] args) {
        
        Scanner clavier= new Scanner(System.in);
        
        float ca;
        float com=0f;
        
        System.out.println("Chiffre d'affaires réalisé? ");
        ca=clavier.nextFloat();
       
        
        if      ( ca < 10000) { com= ca*0.02f; }  
        else if ( ca < 20000) { com = 200 +(ca-10000)*0.04f; }
        else                  { com = 600 +(ca-20000)*0.06f; }
            
        System.out.println("Montant de la commission: "+com+ " €");
    }

}
