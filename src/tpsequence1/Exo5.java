package exercices;

import java.util.Scanner;

public class Exo5 {

    public static void main(String[] args) {
        float taille, poids, imc;
        Scanner scan = new Scanner(System.in);
        
        System.out.print("Quel est votre poids? ");
        poids=scan.nextFloat();
        
        System.out.print("Quel est votre taille? ");
        taille=scan.nextFloat();
        
        imc=10000*poids/(taille*taille);
        
        if (imc<19 && imc>0) {
            System.out.println("Votre imc est de : " + imc + ". Catégorie : Maigreur");
        } else if (imc>=19 && imc<25) {
            System.out.println("Votre imc est de : " + imc + ". Catégorie : Normal");
        } else if (imc>=25 && imc<30) {
            System.out.println("Votre imc est de : " + imc + ". Catégorie : Surpoids");
        } else if (imc>=30) {
            System.out.println("Votre imc est de : " + imc + ". Catégorie : Surpoids");
        } else {
            System.out.println("IMC incalculable, veuillez vérifier les valeur que vous avez rentré");
        }
        
    }
}
