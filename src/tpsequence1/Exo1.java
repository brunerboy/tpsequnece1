package exercices;

import java.util.Scanner;

public class Exo1 {
    public static void main (String[] args) {
        float lon, lar, peri, surf;
        Scanner scan = new Scanner(System.in);
        
        System.out.print("Entrez une longueur : ");
        lon = scan.nextFloat();
        System.out.print("Entrez une largeur : ");
        lar = scan.nextFloat();
        
        peri= lon*2 + lar*2;
        surf= lon*lar;
        
        System.out.println("Le périmètre est : " + peri);
        System.out.println("La surface est : " + surf);
    }
}
