package programmes;

import java.util.Scanner;

public class ExoComplementaire {

    
    public static void main(String[] args) {
          Scanner clavier= new Scanner (System.in);
          
          int taille, age, poids, sexe;
          float IMG;

          System.out.print("Quelle est votre taille en cm ? ");
          taille = clavier.nextInt();
          
          System.out.print("Quel est votre âge ? ");
          age=clavier.nextInt();
          
          System.out.print("Quel est votre poids ? ");
          poids=clavier.nextInt();
          
          System.out.print("Quel est votre sexe (0 pour F ou 1 pour M) ? ");
          sexe=clavier.nextInt();

          IMG=(float)((1.2*(10000*poids/(taille*taille)))+(0.23*age)-(10.8*sexe)-5.4);
          System.out.printf("Votre IMG est de %3.1f",IMG);

          if(sexe==0)
          {
              if(IMG<25)
              {
                  System.out.println("Trop Maigre");
              }
              if(IMG>=25 && IMG<30)
              {
                  System.out.println("Normal");
              }
              
              if(IMG>30)
              {
                  System.out.println("Trop de graisse");
              }
          }
          
          if(sexe==1)
          {
              if(IMG<15)
              {
                  System.out.println("Trop Maigre");
              }
              if(IMG>=15 && IMG<20)
              {
                  System.out.println("Normal");
              }
              
              if(IMG>20)
              {
                  System.out.println("Trop de graisse");
              }
          }
    }
}
