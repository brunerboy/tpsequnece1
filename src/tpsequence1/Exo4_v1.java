
package exercices;

import java.util.Scanner;

public class Exo4_v1 {
 
    public static void main(String[] args) {
        
        Scanner clavier= new Scanner(System.in);
        
        float ca=clavier.nextFloat();
        float com=0f;
        
        if ( ca < 10000){
        
            com= ca*0.02f;  
        }
        else{
            
            if ( ca <20000){
              
                com = 200f +(ca-10000f)*0.04f;
            }
            else{
                com = 600f +(ca-20000f)*0.06f;
            }
        }
        
        System.out.println("Montant de votre commission: "+com+ " €");
    }

}
