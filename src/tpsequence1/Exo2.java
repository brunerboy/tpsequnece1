package exercices;

import java.util.Scanner;

public class Exo2 {
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int age;
        
        System.out.println("Quel est votre âge? ");
        age=scan.nextInt();
        
        if (age>=18) {
            System.out.println("Vous êtes majeur");
        } else {
            System.out.println("Vous êtes mineur");
        }
    }
}
