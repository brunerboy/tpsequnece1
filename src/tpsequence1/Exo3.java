package exercices;

import java.util.Scanner;

public class Exo3 {
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        float ca, com;
        
        System.out.print("Quel est votre chiffre d'affaire? ");
        ca=scan.nextFloat();
        
        if (ca<10000) {
            com=ca*0.02f;
        } else {
            com=200+(ca-10000)*0.04f;
        }
        
        System.out.println("Votre commission est de : "+com+"€");
    }
}
