package exercices;

import java.util.Scanner;

public class Exo9 {
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        float moy=0, note=0, max=0, min=20, total=0;
        int cpt=0;
        
        System.out.println("Entrer vos notes une par une (-1 quand vous avez finis) :");
        note=scan.nextFloat();
        
        while (note != -1) {
            total+=note;
            if (note>max) {
                max=note;
            }
            if (note<min) {
                min=note;
            }
            cpt++;
            note=scan.nextFloat();
        }
        
        moy=total/cpt;
        
        System.out.println("Votre moyenne est de : "+moy);
        System.out.println("Votre note maximum est : "+max);
        System.out.println("Votre note minimum est : "+min);
        
    }
    
}
